# Instruções

#### Abaixo segue as instruções de configuração para rodar o teste proposto no desafio.

1. Clonar o repositório [Gitlab](https://gitlab.com/alvarengadouglas/opencart)
2. Criar conta no [BrowserStack](https://www.browserstack.com/) 
3. Dentro do projeto no caminho src/test/java/support existe uma classe Browser.java,
    será necessário alterar as credencias de acesso ao serviço de testes em nuvem do BrowserStack.
    Suas credenciais ficam disponíveis após o cadastro feito no browser stack.
    
    ```
    public class Browser {
    //credenciais browserstack
    public static final String USERNAME = "automacaodeteste1";
    public static final String AUTOMATE_KEY = "CaKArag9honUS1P67uPw";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    ```
    No código disponibilizado foi criada uma conta para uso no desafio, para obter uma completa análise e evidência em vídeo do teste executado. 
4. A classe de teste a ser executada está no caminho src/test/java/tests e o nome da classe é TestsNewUserLogoutAndLogin.java
5. Para criar sempre usuários novos sem precisar alteração manual ou massa de dados, no e-mail de usuário foi utilizado um timestamp,
    para rodar o teste e obter sucesso é necessário que ele seja feito ao início e um novo minuto, para que o usuário que irá se cadastrar 
    tenha o mesmo e-mail do que vai logar ao final do teste. O teste leva cerca de 25 segundos ser finalizado.
    Ex.: Rodar o teste as 17 horas 40 minutos e 10 segundos
