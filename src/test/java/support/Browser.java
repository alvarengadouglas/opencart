package support;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Browser {
	//credenciais browserstack
	public static final String USERNAME = "automacaodeteste1";
	public static final String AUTOMATE_KEY = "CaKArag9honUS1P67uPw";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
	private static WebDriver driver = null;
	
	//config
	public static WebDriver createBrowserStack() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("os_version", "High Sierra");
	    caps.setCapability("resolution", "1920x1080");
	    caps.setCapability("browser", "Chrome");
	    caps.setCapability("browser_version", "83.0");
	    caps.setCapability("os", "OS X");
		
		try {
			//instancia e configuracao do browser
			driver = new RemoteWebDriver(new URL(URL), caps);
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
			//url raiz, ponto de partida da automacao
			driver.get("https://demo.opencart.com/");
		} catch (Exception e) {
			System.out.println("Houve um problema com a URL: " + e.getMessage());
		}
		
		return driver;
	}	
	
	public static void quitBrowser() {
		driver.quit();
	}

}
