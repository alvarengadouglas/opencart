package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pages.FormRegister;
import support.Browser;

public class TestsNewUserLogoutAndLogin {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Browser.quitBrowser();	
	}

	@Test
	public void testCriarUsuarioLogarDeslogar() {
		FormRegister home = new FormRegister(Browser.createBrowserStack());
		home.cadastrarNovoUsuario()
		.logout()
		.realizarLogin();
	}

}
