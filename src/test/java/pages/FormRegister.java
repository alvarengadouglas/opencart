package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import support.Generator;

public class FormRegister {
	private WebDriver driver;
	private String password = "abc123";
	private String email = "javacoffeeautomacao" + Generator.dataHoraParaEmailSemDuplicacao() + "@test.com";;

	public FormRegister(WebDriver driver) {
		this.driver = driver;
	}

	// realizar cadastro e validar mensagem de sucesso
	public FormRegister cadastrarNovoUsuario() {
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Register")).click();
		driver.findElement(By.id("input-firstname")).sendKeys("Java");
		driver.findElement(By.id("input-lastname")).sendKeys("Coffee");
		driver.findElement(By.id("input-email")).sendKeys(this.email);
		driver.findElement(By.id("input-telephone")).sendKeys("(51)99999-8877");
		driver.findElement(By.id("input-password")).sendKeys(this.password);
		driver.findElement(By.id("input-confirm")).sendKeys(this.password);
		driver.findElement(By.name("agree")).click();
		driver.findElement(By.xpath("//input[@type='submit'][@value='Continue']")).click();
		String msg = driver.findElement(By.xpath("//div[@id='content']/h1")).getText();
		assertEquals("Your Account Has Been Created!", msg);

		return new FormRegister(driver);
	}


	// faz logout na aplicacao
	public FormRegister logout() {
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Logout")).click();

		return new FormRegister(driver);
	}
	
	//fazer login na aplicacao com dados do cadastro
	public FormRegister realizarLogin() {
		driver.findElement(By.linkText("My Account")).click();
		driver.findElement(By.linkText("Login")).click();
		driver.findElement(By.id("input-email")).sendKeys(this.email);
		driver.findElement(By.id("input-password")).sendKeys(this.password);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		String url = driver.getCurrentUrl();
		assertEquals("https://demo.opencart.com/index.php?route=account/account", url);
		
		return new FormRegister(driver);
	}

}
